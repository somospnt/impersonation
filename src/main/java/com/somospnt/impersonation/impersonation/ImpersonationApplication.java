package com.somospnt.impersonation.impersonation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImpersonationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImpersonationApplication.class, args);
	}
}
